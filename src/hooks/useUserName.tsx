import {useEffect, useState} from 'react';
import {AppStorage} from '../utils/AppStorage';
import {APP_IS_LOGGED_IN_STORAGE_KEY} from '../utils/constants';

export const useUserName = () => {
  const [userName, setUserName] = useState('');

  useEffect(() => {
    const getUserName = async () => {
      setUserName(await AppStorage.getItem(APP_IS_LOGGED_IN_STORAGE_KEY));
    };

    getUserName();
  }, []);

  return {userName};
};
