import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Screens} from './Screens';
import {LoginScreen} from '../screens/LoginScreen';
import {SignUpScreen} from '../screens/SignUpScreen';
import {WelcomeScreen} from '../screens/WelcomeScreen';

const LoginStack = createStackNavigator();

export const LoginStackNavigator = () => {
  return (
    <LoginStack.Navigator>
      <LoginStack.Screen
        name={Screens.WelcomeScreen}
        component={WelcomeScreen}
        options={{headerShown: false, gestureEnabled: false}}
      />
      <LoginStack.Screen
        name={Screens.LoginScreen}
        component={LoginScreen}
        options={{gestureEnabled: false, headerLeft: () => null}}
      />
      <LoginStack.Screen name={Screens.SignUpScreen} component={SignUpScreen} />
    </LoginStack.Navigator>
  );
};
