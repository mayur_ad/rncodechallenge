export const Screens = {
  DefaultStack: 'DefaultStack',
  BottomTab: 'BottomTab',
  LoginStack: 'LoginStack',
  LoginScreen: 'Login',
  SignUpScreen: 'Sign Up',
  WelcomeScreen: 'WelcomeScreen',
  HomeScreenTab1: 'HomeScreenTab1',
  HomeScreenTab2: 'HomeScreenTab2',
  HomeScreenTab3: 'HomeScreenTab3',
  HomeScreenTab4: 'HomeScreenTab4',
};
