import React, {useEffect} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Screens} from './Screens';
import {LoginStackNavigator} from './LoginStackNavigator';
import {WelcomeScreen} from '../screens/WelcomeScreen';
import {useContextSelector} from 'use-context-selector';
import {
  AuthContext,
  authDispatchSelector,
  checkIsLoggedIn,
  loggedInStatusSelector,
} from '../context/auth/AuthContext';
import {setLoggedInStatus} from '../context/auth/AuthActions';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {HomeScreenTab1} from '../screens/HomeScreenTab1';
import {HomeScreenTab2} from '../screens/HomeScreenTab2';
import {HomeScreenTab3} from '../screens/HomeScreenTab3';
import {HomeScreenTab4} from '../screens/HomeScreenTab4';
import RNBootSplash from 'react-native-bootsplash';

const Stack = createStackNavigator();
const BottomTab = createBottomTabNavigator();

export const MainNavigator = () => {
  const isLoggedIn = useContextSelector(AuthContext, loggedInStatusSelector);
  const authDispatch = useContextSelector(AuthContext, authDispatchSelector);

  useEffect(() => {
    const checkLoggedInStatus = async () => {
      authDispatch(setLoggedInStatus(await checkIsLoggedIn()));
      setTimeout(() => {
        RNBootSplash.hide();
      }, 500);
    };

    checkLoggedInStatus();
  }, []);

  return (
    <>
      {isLoggedIn ? (
        <Stack.Navigator>
          <Stack.Screen
            name={Screens.BottomTab}
            component={BottomTabNavigator}
            options={{headerTitle: 'Home'}}
          />
        </Stack.Navigator>
      ) : (
        <Stack.Navigator>
          <Stack.Screen
            name={Screens.LoginStack}
            component={LoginStackNavigator}
            options={{headerShown: false}}
          />
        </Stack.Navigator>
      )}
    </>
  );
};

const BottomTabNavigator = () => {
  return (
    <BottomTab.Navigator
      tabBarOptions={{
        inactiveBackgroundColor: '#ccc',
        inactiveTintColor: '#444',
        tabStyle: {justifyContent: 'center'},
      }}>
      <BottomTab.Screen
        name={Screens.HomeScreenTab1}
        component={HomeScreenTab1}
        options={{title: 'Tab1'}}
      />
      <BottomTab.Screen
        name={Screens.HomeScreenTab2}
        component={HomeScreenTab2}
        options={{title: 'Tab2'}}
      />
      <BottomTab.Screen
        name={Screens.HomeScreenTab3}
        component={HomeScreenTab3}
        options={{title: 'Tab3'}}
      />
      <BottomTab.Screen
        name={Screens.HomeScreenTab4}
        component={HomeScreenTab4}
        options={{title: 'Tab4'}}
      />
    </BottomTab.Navigator>
  );
};
