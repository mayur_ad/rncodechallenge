export default class User {
  userName: string;
  mobileNumber: string;
  emailId: string;
  password: string;
}
