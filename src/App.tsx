import 'react-native-gesture-handler';
import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {navigationRef} from './navigation/NavigationActions';
import {AuthProvider} from './context/auth/AuthContext';
import {MainNavigator} from './navigation/MainNavigator';

const App = () => {
  return (
    <AuthProvider>
      <NavigationContainer ref={navigationRef}>
        <MainNavigator />
      </NavigationContainer>
    </AuthProvider>
  );
};

export default App;
