import React, {useState} from 'react';
import {ScrollView, ActivityIndicator} from 'react-native';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {ErrorMessageText} from '../components/ErrorMessageText';
import {ButtonPrimary} from '../components/ButtonPrimary';
import {navigate} from '../navigation/NavigationActions';
import {Screens} from '../navigation/Screens';
import {AppStorage} from '../utils/AppStorage';
import {APP_SIGNED_UP_USERS_STORAGE_KEY} from '../utils/constants';
import User from '../types/User';
import {KeyboardAwareView} from 'react-native-keyboard-aware-view';
import {CustomForm} from '../components/CustomForm';
import {CustomTextInput} from '../components/CustomTextInput';

export const SignUpScreen = () => {
  const [isEmailIdExist, setIsEmailIdExist] = useState(false);
  const [isMobileNumberExist, setIsMobileNumberExist] = useState(false);
  const [isUserNameExist, setIsUserNameExist] = useState(false);

  const [isLoading, setLoading] = useState(false);

  const signUpValidationSchema = Yup.object().shape({
    userName: Yup.string()
      .required('Please enter username')
      .min(3, 'Please enter at least 3 characters'),
    mobileNumber: Yup.string()
      .required('Please enter Mobile Number')
      .matches(
        /^(0|[+91]{3})?[1-9][0-9]{9}$/,
        'Please enter valid Mobile Number',
      ),
    emailId: Yup.string()
      .required('Please enter Email Id')
      .email('Please enter valid Email Id'),
    password: Yup.string()
      .required('Please enter password')
      .min(5, 'Please enter at least 5 characters'),
  });

  const signUp = async (
    userName: string,
    mobileNumber: string,
    emailId: string,
    password: string,
  ) => {
    setLoading(true);
    const signedUpData = (await AppStorage.getItem(
      APP_SIGNED_UP_USERS_STORAGE_KEY,
    )) as User[];

    if (signedUpData) {
      const isDataExist = signedUpData.find((value: User) => {
        const isEmailId = value.emailId === emailId;
        const isMobileNumber = value.mobileNumber === mobileNumber;
        const isUserName = value.userName === userName;

        setIsEmailIdExist(isEmailId);
        setIsMobileNumberExist(isMobileNumber);
        setIsUserNameExist(isUserName);

        return isEmailId || isMobileNumber || isUserName;
      });

      if (!isDataExist) {
        await AppStorage.setItem(
          APP_SIGNED_UP_USERS_STORAGE_KEY,
          JSON.stringify(
            signedUpData.concat({
              userName: userName,
              mobileNumber: mobileNumber,
              emailId: emailId,
              password: password,
            }),
          ),
        );
        navigate(Screens.LoginScreen);
      }
    } else {
      await AppStorage.setItem(
        APP_SIGNED_UP_USERS_STORAGE_KEY,
        JSON.stringify([
          {
            userName: userName,
            mobileNumber: mobileNumber,
            emailId: emailId,
            password: password,
          },
        ]),
      );
      navigate(Screens.LoginScreen);
    }
    setLoading(false);
  };

  return (
    <KeyboardAwareView
      animated={true}
      doNotForceDismissKeyboardWhenLayoutChanges={true}>
      <ScrollView keyboardShouldPersistTaps="handled" style={{padding: 20}}>
        <Formik
          initialValues={{
            userName: '',
            mobileNumber: '',
            emailId: '',
            password: '',
          }}
          validationSchema={signUpValidationSchema}
          onSubmit={values => {
            signUp(
              values.userName,
              values.mobileNumber,
              values.emailId,
              values.password,
            );
          }}>
          {({handleChange, submitForm, values, errors, touched}) => (
            <CustomForm>
              <CustomTextInput
                text={'User Name'}
                onChangeText={handleChange('userName')}
                value={values.userName}
                onKeyPress={() => setIsUserNameExist(false)}
              />

              {touched.userName && errors.userName && (
                <ErrorMessageText text={errors.userName} />
              )}

              {isUserNameExist && (
                <ErrorMessageText text={'User Name already taken'} />
              )}

              <CustomTextInput
                text={'Mobile Number'}
                onChangeText={handleChange('mobileNumber')}
                value={values.mobileNumber}
                onKeyPress={() => {
                  setIsMobileNumberExist(false);
                }}
              />

              {touched.mobileNumber && errors.mobileNumber && (
                <ErrorMessageText text={errors.mobileNumber} />
              )}

              {isMobileNumberExist && (
                <ErrorMessageText text={'Mobile Number already taken'} />
              )}

              <CustomTextInput
                text={'Email Id'}
                keyboardType={'email-address'}
                onChangeText={handleChange('emailId')}
                value={values.emailId}
                onKeyPress={() => setIsEmailIdExist(false)}
              />

              {touched.emailId && errors.emailId && (
                <ErrorMessageText text={errors.emailId} />
              )}

              {isEmailIdExist && (
                <ErrorMessageText text={'Email Id already taken'} />
              )}

              <CustomTextInput
                text={'Password'}
                onChangeText={handleChange('password')}
                value={values.password}
                isPassword={true}
              />

              {touched.password && errors.password && (
                <ErrorMessageText text={errors.password} />
              )}

              <ButtonPrimary text={'Sign Up'} onPress={submitForm} />
            </CustomForm>
          )}
        </Formik>
        {isLoading && <ActivityIndicator color={'blue'} size={'large'} />}
      </ScrollView>
    </KeyboardAwareView>
  );
};
