import React from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';
import Swiper from 'react-native-swiper';
import {navigate} from '../navigation/NavigationActions';
import {Screens} from '../navigation/Screens';

export const WelcomeScreen = () => {
  return (
    <>
      <Swiper showsButtons={true}>
        <View style={styles.slide}>
          <Text style={[styles.text]}>Welcome</Text>
          <View style={{marginTop: 20}}>
            <Button
              title={'Skip Introduction'}
              onPress={() => {
                navigate(Screens.LoginScreen);
              }}
            />
          </View>
        </View>
        <View style={styles.slide}>
          <Text style={styles.text}>Introduction</Text>
        </View>
        <View style={styles.slide}>
          <Text style={styles.text}>Final Slide</Text>
          <View style={{marginTop: 20}}>
            <Button
              title={'Go to Login'}
              onPress={() => {
                navigate(Screens.LoginScreen);
              }}
            />
          </View>
        </View>
      </Swiper>
    </>
  );
};

const styles = StyleSheet.create({
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#e0f2ff',
  },
  text: {
    color: '#000',
    fontSize: 30,
  },
});
