import React, {useState} from 'react';
import {ActivityIndicator, ScrollView, BackHandler} from 'react-native';
import {useContextSelector} from 'use-context-selector';
import {AuthContext, authDispatchSelector} from '../context/auth/AuthContext';
import {setLoggedInStatus} from '../context/auth/AuthActions';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {CustomTextInput} from '../components/CustomTextInput';
import {ErrorMessageText} from '../components/ErrorMessageText';
import {ButtonPrimary} from '../components/ButtonPrimary';
import {ButtonSecondary} from '../components/ButtonSecondary';
import {navigate} from '../navigation/NavigationActions';
import {Screens} from '../navigation/Screens';
import {AppStorage} from '../utils/AppStorage';
import {
  APP_IS_LOGGED_IN_STORAGE_KEY,
  APP_SIGNED_UP_USERS_STORAGE_KEY,
} from '../utils/constants';
import User from '../types/User';
import {useFocusEffect} from '@react-navigation/core';
import {KeyboardAwareView} from 'react-native-keyboard-aware-view';
import {CustomForm} from '../components/CustomForm';

export const LoginScreen = () => {
  const [isLoginError, setIsLoginError] = useState(false);
  const [isLoading, setLoading] = useState(false);

  const authDispatch = useContextSelector(AuthContext, authDispatchSelector);

  const loginValidationSchema = Yup.object().shape({
    userName: Yup.string().required('Please enter username'),
    password: Yup.string().required('Please enter password'),
  });

  const login = async (userName: string, password: string) => {
    setLoading(true);
    const signedUpData = (await AppStorage.getItem(
      APP_SIGNED_UP_USERS_STORAGE_KEY,
    )) as User[];

    if (signedUpData) {
      const isDataExist = signedUpData.find(
        (value: User) =>
          value.userName === userName && value.password === password,
      );

      if (isDataExist) {
        await AppStorage.setItem(APP_IS_LOGGED_IN_STORAGE_KEY, userName);
        authDispatch(setLoggedInStatus(true));
      } else {
        setIsLoginError(true);
      }
    } else {
      setIsLoginError(true);
    }
    setLoading(false);
  };

  useFocusEffect(
    React.useCallback(() => {
      setIsLoginError(false);
    }, []),
  );

  useFocusEffect(
    React.useCallback(() => {
      const onBackPress = () => {
        BackHandler.exitApp();
        return true;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, []),
  );

  return (
    <KeyboardAwareView
      animated={true}
      doNotForceDismissKeyboardWhenLayoutChanges={true}>
      <ScrollView
        keyboardShouldPersistTaps="handled"
        style={{paddingHorizontal: 20, paddingTop: '20%'}}>
        <Formik
          initialValues={{userName: '', password: ''}}
          validationSchema={loginValidationSchema}
          onSubmit={values => {
            login(values.userName, values.password);
          }}>
          {({handleChange, submitForm, values, errors, touched}) => (
            <CustomForm>
              <CustomTextInput
                text={'User Name'}
                onChangeText={handleChange('userName')}
                value={values.userName}
                onKeyPress={() => setIsLoginError(false)}
              />

              {touched.userName && errors.userName && (
                <ErrorMessageText text={errors.userName} />
              )}

              <CustomTextInput
                text={'Password'}
                onChangeText={handleChange('password')}
                isPassword={true}
                value={values.password}
                onKeyPress={() => setIsLoginError(false)}
              />

              {touched.password && errors.password && (
                <ErrorMessageText text={errors.password} />
              )}

              {isLoginError && (
                <ErrorMessageText
                  text={'User not found, please register using Sign Up'}
                />
              )}

              <ButtonPrimary text={'Login'} onPress={submitForm} />

              <ButtonSecondary
                text={'Sign Up'}
                onPress={() => navigate(Screens.SignUpScreen)}
              />
            </CustomForm>
          )}
        </Formik>
        {isLoading && <ActivityIndicator color={'blue'} size={'large'} />}
      </ScrollView>
    </KeyboardAwareView>
  );
};
