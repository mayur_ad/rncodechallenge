import React from 'react';
import {Text, View} from 'react-native';
import {useContextSelector} from 'use-context-selector';
import {ButtonPrimary} from '../components/ButtonPrimary';
import {ButtonSecondary} from '../components/ButtonSecondary';
import {setLoggedInStatus} from '../context/auth/AuthActions';
import {AuthContext, authDispatchSelector} from '../context/auth/AuthContext';
import {useUserName} from '../hooks/useUserName';
import {AppStorage} from '../utils/AppStorage';
import {APP_IS_LOGGED_IN_STORAGE_KEY} from '../utils/constants';
import Share from 'react-native-share';

export const HomeScreenTab1 = () => {
  const {userName} = useUserName();
  const authDispatch = useContextSelector(AuthContext, authDispatchSelector);

  const logout = React.useCallback(async () => {
    await AppStorage.removeItem(APP_IS_LOGGED_IN_STORAGE_KEY);
    authDispatch(setLoggedInStatus(false));
  }, []);

  const shareVideo = React.useCallback(async () => {
    try {
      await Share.open({
        url: 'https://youtu.be/NCAY0HIfrwc',
      });
    } catch (e) {}
  }, []);

  return (
    <View
      style={{
        flex: 1,
        padding: 20,
        paddingTop: '20%',
      }}>
      <Text style={{fontSize: 20}}>Hello, {userName}</Text>

      <ButtonPrimary text={'Share Video'} onPress={shareVideo} />

      <ButtonSecondary text={'Logout'} onPress={logout} />
    </View>
  );
};
