import React from 'react';
import {Text, View} from 'react-native';

export const HomeScreenTab3 = () => {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text style={{fontSize: 30, color: '#7c149c'}}>Tab 3</Text>
    </View>
  );
};
