import React from 'react';
import {Text, View} from 'react-native';

export const HomeScreenTab4 = () => {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text style={{fontSize: 30, color: '#800e27'}}>Tab 4</Text>
    </View>
  );
};
