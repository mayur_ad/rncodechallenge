import {StyleSheet} from 'react-native';

export const commonStyles = StyleSheet.create({
  textInput: {
    borderWidth: 1,
    borderColor: 'lightblue',
    height: 50,
    backgroundColor: '#fff',
    marginTop: 5,
    marginBottom: 5,
    color: '#000',
    paddingHorizontal: 10,
  },
  errorMessage: {
    color: '#ff0000',
    marginLeft: 2,
  },
  touchableOpacity: {
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#3486eb',
    marginVertical: 20,
  },
});
