import AsyncStorage from '@react-native-async-storage/async-storage';

export namespace AppStorage {
  export const getItem = async (key: string) => {
    try {
      const val = await AsyncStorage.getItem(key);
      return JSON.parse(val);
    } catch (e) {
      return await AsyncStorage.getItem(key);
    }
  };

  export const setItem = async (key: string, val: string) => {
    try {
      return await AsyncStorage.setItem(key, val);
    } catch (e) {
      return;
    }
  };

  export const removeItem = async (key: string): Promise<void> => {
    try {
      return await AsyncStorage.removeItem(key);
    } catch (e) {
      return;
    }
  };
}
