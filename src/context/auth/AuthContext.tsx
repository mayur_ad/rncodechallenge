import React, {PropsWithChildren, useReducer, ReactNode} from 'react';
import {createContext} from 'use-context-selector';
import {AuthStateType, AuthContextType, AuthActionType} from './AuthStateTypes';
import {AuthReducer} from './AuthReducer';
import {AppStorage} from '../../utils/AppStorage';
import {APP_IS_LOGGED_IN_STORAGE_KEY} from '../../utils/constants';

const initialState = {
  isLoggedIn: false,
  isFirstLoggedIn: false,
  authUser: {},
} as AuthStateType;

const AuthContext = createContext<AuthContextType>({
  state: initialState,
  dispatch: () => null,
});

const AuthProvider = (props: PropsWithChildren<ReactNode>) => {
  const [state, dispatch] = useReducer(AuthReducer, initialState);

  return (
    <AuthContext.Provider
      value={{
        state,
        dispatch,
      }}>
      {props.children}
    </AuthContext.Provider>
  );
};

export {AuthProvider, AuthContext};

export const authDispatchSelector = (
  authContext: AuthContextType,
): ((action: AuthActionType) => void) => authContext.dispatch;

export const loggedInStatusSelector = (authContext: AuthContextType): boolean =>
  authContext.state.isLoggedIn;

export const checkIsLoggedIn = async () =>
  !!(await AppStorage.getItem(APP_IS_LOGGED_IN_STORAGE_KEY));
