import User from '../../types/User';
import {allAuthActions} from './AuthActions';

export type AuthStateType = {
  isLoggedIn: boolean;
  isFirstLoggedIn: boolean;
  authUser: User;
};

export type AuthActionType = typeof allAuthActions.actions;

export type AuthContextType = {
  state: AuthStateType;
  dispatch: (action: AuthActionType) => void;
};
