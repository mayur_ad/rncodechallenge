import {AuthStateType, AuthActionType} from './AuthStateTypes';
import {setLoggedInStatus} from './AuthActions';

export const AuthReducer = (
  state: AuthStateType,
  action: AuthActionType,
): AuthStateType => {
  switch (action.type) {
    case setLoggedInStatus.type:
      return handleSetLoggedInStatus(state, action.loggedInStatus);

    default:
      return state;
  }
};

const handleSetLoggedInStatus = (
  state: AuthStateType,
  loggedInStatus: boolean,
) => {
  return {
    ...state,
    isLoggedIn: loggedInStatus,
  };
};
