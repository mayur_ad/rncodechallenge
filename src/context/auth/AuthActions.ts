import {action, union} from 'ts-action';

export const setLoggedInStatus = action(
  'SET_LOGGED_IN_STATUS',
  (loggedInStatus: boolean) => ({loggedInStatus}),
);

export const allAuthActions = union(setLoggedInStatus);
