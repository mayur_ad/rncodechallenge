import React from 'react';
import {TouchableOpacity, TouchableOpacityProps, Text} from 'react-native';
import {commonStyles} from '../styles/commonStyles';

export const ButtonSecondary = (
  props: {text: string} & TouchableOpacityProps,
) => {
  return (
    <TouchableOpacity
      style={[
        commonStyles.touchableOpacity,
        {marginTop: 0, backgroundColor: 'transparent'},
      ]}
      onPress={props.onPress}>
      <Text
        style={{
          color: '#3486eb',
          fontSize: 20,
          borderBottomWidth: 1,
          borderBottomColor: '#3486eb',
        }}>
        {props.text}
      </Text>
    </TouchableOpacity>
  );
};
