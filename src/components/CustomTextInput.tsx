import React from 'react';
import {Text, TextInput, TextInputProps, View} from 'react-native';
import {commonStyles} from '../styles/commonStyles';

export const CustomTextInput = (
  props: {text: string; isPassword?: boolean} & TextInputProps,
) => {
  return (
    <View style={{marginTop: 10}}>
      <Text style={{marginLeft: 2}}>{props.text}</Text>
      <TextInput
        style={commonStyles.textInput}
        onChangeText={props.onChangeText}
        value={props.value}
        secureTextEntry={props.isPassword}
        {...props}
      />
    </View>
  );
};
