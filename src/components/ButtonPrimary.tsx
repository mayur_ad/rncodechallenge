import React from 'react';
import {TouchableOpacity, TouchableOpacityProps, Text} from 'react-native';
import {commonStyles} from '../styles/commonStyles';

export const ButtonPrimary = (
  props: {text: String} & TouchableOpacityProps,
) => {
  return (
    <TouchableOpacity
      style={[{width: '100%'}, commonStyles.touchableOpacity]}
      onPress={props.onPress}>
      <Text style={{color: '#fff', fontSize: 20}}>{props.text}</Text>
    </TouchableOpacity>
  );
};
