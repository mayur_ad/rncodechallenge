import React from 'react';
import {Text} from 'react-native';
import {commonStyles} from '../styles/commonStyles';

export const ErrorMessageText = (props: {text: string}) => {
  return <Text style={commonStyles.errorMessage}>{props.text}</Text>;
};
