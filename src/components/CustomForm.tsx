import {View} from 'react-native';
import {withNextInputAutoFocusForm} from 'react-native-formik/index';

export const CustomForm = withNextInputAutoFocusForm(View);
